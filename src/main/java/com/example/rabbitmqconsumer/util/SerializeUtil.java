package com.example.rabbitmqconsumer.util;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;

public class SerializeUtil {
    private static Logger log = LoggerFactory.getLogger(SerializeUtil.class);

    /**
     * 	序列化
     * @param object
     * @return
     * @throws IOException
     */
    public static byte[] serialize(Object object) {
        ObjectOutputStream oos = null;
        ByteArrayOutputStream baos = null;
        byte[] rtnByte = null;
        try {
            baos = new ByteArrayOutputStream();
            oos = new ObjectOutputStream(baos);
            oos.writeObject(object);
            oos.flush();
            oos.close();
            rtnByte = baos.toByteArray();
        } catch (Exception e) {
            log.debug(ExceptionUtils.getStackTrace(e));
        }
        return rtnByte;
    }

    /**
     * 	反序列化
     * @param bytes
     * @return
     * @throws IOException
     */
    public static Object deserialize(byte[] bytes) {
        ByteArrayInputStream bais = null;
        ObjectInputStream ois = null;
        Object obj = null;
        try {
            bais = new ByteArrayInputStream(bytes);
            ois = new ObjectInputStream(bais);
            obj = ois.readObject();
            ois.close();
            return obj;
        } catch (Exception e) {
            log.debug(ExceptionUtils.getStackTrace(e));
        }
        return null;
    }
}
