package com.example.rabbitmqconsumer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = {"com.example.rabbitmqconsumer"})
public class RabbitmqconsumerApplication {
    public static void main(String[] args) {
        SpringApplication.run(RabbitmqconsumerApplication.class, args);
    }
}
