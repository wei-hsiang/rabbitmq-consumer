package com.example.rabbitmqconsumer.handler;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.amqp.support.converter.MessageConversionException;
import org.springframework.amqp.support.converter.MessageConverter;

public class TextMessageConverter implements MessageConverter {

    private static Logger log = LoggerFactory.getLogger(TextMessageConverter.class);

    @Override
    public Message toMessage(Object o, MessageProperties messageProperties) throws MessageConversionException {
        return new Message(o.toString().getBytes(), messageProperties);
    }

    @Override
    public Object fromMessage(Message message) throws MessageConversionException {
        String contenType = message.getMessageProperties().getContentType();
        log.debug("===" + contenType + "===");
        if (StringUtils.isNotBlank(contenType) && contenType.contains("text")) {
            return new String(message.getBody());
        }
        return message.getBody();
    }
}
