package com.example.rabbitmqconsumer.handler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.List;
import java.util.Map;

public class MessageDelegate {
    private static Logger log = LoggerFactory.getLogger(MessageDelegate.class);
    
    public void onMessage(byte[] message){
        log.debug("debug---------onMessage----byte-------------");
        log.debug(new String(message));
    }

    public void onMessage(String message){
        log.debug("---------onMessage---String-------------");
        log.debug(message);
    }

    public void onMessage(List<Object> orders){
        log.debug("---------onMessage---List<Order>-------------");
        orders.stream().forEach(order -> log.debug(String.valueOf(order)));
    }

    public void onMessage(Map<String,Object> orderMaps){
        log.debug("-------onMessage---Map<String,Object>------------");
        orderMaps.keySet().forEach(key -> log.debug(key.toString()));
    }

    public void onMessage(File message){
        log.debug("-------onMessage---File message------------");
        log.debug(message.getName());
    }
}
